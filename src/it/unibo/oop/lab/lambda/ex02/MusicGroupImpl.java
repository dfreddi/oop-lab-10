package it.unibo.oop.lab.lambda.ex02;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.Set;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 *
 */
public final class MusicGroupImpl implements MusicGroup {

    private final Map<String, Integer> albums = new HashMap<>();
    private final Set<Song> songs = new HashSet<>();

    @Override
    public void addAlbum(final String albumName, final int year) {
        this.albums.put(albumName, year);
    }

    @Override
    public void addSong(final String songName, final Optional<String> albumName, final double duration) {
        if (albumName.isPresent() && !this.albums.containsKey(albumName.get())) {
            throw new IllegalArgumentException("invalid album name");
        }
        this.songs.add(new MusicGroupImpl.Song(songName, albumName, duration));
    }

    @Override
    public Stream<String> orderedSongNames() {
        return songs.stream().map(song -> song.songName).sorted();
    }

    @Override
    public Stream<String> albumNames() {
        return albums.keySet().stream();
    }

    @Override
    public Stream<String> albumInYear(final int year) {
        return albumNames().filter(key -> albums.get(key).equals(year));
    }

    private Stream<Song> songsInAlbum(final String albumName) {
        return songs.stream().filter(song -> song.albumName.isPresent() && song.albumName.get().equals(albumName));
    }

    @Override
    public int countSongs(final String albumName) {
        return (int) songsInAlbum(albumName).count();
    }

    @Override
    public int countSongsInNoAlbum() {
        return (int) songs.stream()
                .filter(song -> song.albumName.isEmpty())
                .count();
    }

    @Override
    public OptionalDouble averageDurationOfSongs(final String albumName) {
        double average = songsInAlbum(albumName)
                .map(Song::getDuration)
                .collect(Collectors.averagingDouble(elem -> elem));
        return average == 0 ? OptionalDouble.empty() : OptionalDouble.of(average);
    }

    @Override
    public Optional<String> longestSong() {
        Optional<Song> song = songs.stream()
                .max((songA, songB) -> (int) (songA.duration - songB.duration));
        return song.map(value -> value.songName);
    }

    @Override
    public Optional<String> longestAlbum() {
        Map<String, Double> albumsWithDuration = new HashMap<>();
        songs.stream().filter(s -> s.albumName.isPresent()).forEach(s -> albumsWithDuration.merge(s.albumName.get(), s.duration, Double::sum));
        return albumsWithDuration.keySet().stream()
                .max((a1, a2) -> (int) (albumsWithDuration.get(a1) - albumsWithDuration.get(a2)));
    }

    private static final class Song {

        private final String songName;
        private final Optional<String> albumName;
        private final double duration;
        private int hash;

        Song(final String name, final Optional<String> album, final double len) {
            super();
            this.songName = name;
            this.albumName = album;
            this.duration = len;
        }

        public String getSongName() {
            return songName;
        }

        public Optional<String> getAlbumName() {
            return albumName;
        }

        public double getDuration() {
            return duration;
        }

        @Override
        public int hashCode() {
            if (hash == 0) {
                hash = songName.hashCode() ^ albumName.hashCode() ^ Double.hashCode(duration);
            }
            return hash;
        }

        @Override
        public boolean equals(final Object obj) {
            if (obj instanceof Song) {
                final Song other = (Song) obj;
                return albumName.equals(other.albumName) && songName.equals(other.songName)
                        && duration == other.duration;
            }
            return false;
        }

        @Override
        public String toString() {
            return "Song [songName=" + songName + ", albumName=" + albumName + ", duration=" + duration + "]";
        }
    }

}
